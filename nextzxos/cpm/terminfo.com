* ��4	 "z��4 "}�2���:�<O/�
0��:!p#w��*�& )�4�^#V��|�($�(
�(O�y�:�=�>�:�<�8�������  �  �$�O<���y�� ��
�W���"q'Z.x2�6�9zE$S0T7ES1T6                   ZX Spectrum Next BIOS Terminal Information         (xx of 13)S0T7

$This program provides information on the terminal facilities provided by the
BIOS on the S0T4ZX Spectrum NextS0T7.

On the S0T4ZX Spectrum NextS0T7, the S1T5EXTENDS0T7 key functions as a control (S1T5CTRLS0T7) key,
so to press S1T5CTRL-SS0T7 (for example), hold down the S1T5EXTENDS0T7 key and press the S1T5SS0T7 key.
You can also hold down S1T5CAPS SHIFTS0T7 and S1T5SYMBOL SHIFTS0T7 together, instead of S1T5EXTENDS0T7.

A few keys have special meanings to this program:

S1T5CTRL-AS0T7 (S1T5Cursor leftS0T7)   Reset the terminal and show the previous screen
S1T5CTRL-FS0T7 (S1T5Cursor rightS0T7)  Reset the terminal and show the next screen
S1T5CTRL-CS0T7                 Exit the program

Any other key pressed whilst this program is active will be sent directly to the
terminal, allowing you to type control codes or escape sequences and see the
effects that they have.

By default, the terminal provided is 24 lines by 80 columns, which is suitable
for most CP/M software. If desired you can change the terminal size using the
S0T4TERMSIZE.COMS0T7 program to anything up to 32 lines by 80 columns.[1;1H$Many CP/M applications need to be configured (via an S0T4INSTALL.COMS0T7 program or
similar) so that they can control the terminal correctly. The S0T4ZX Spectrum Next
S0T7emulates most features of the S0T4Zenith Z-19S0T7 terminal, so choose one of the
following terminal types if possible:

S0T4       Zenith Z-19
       Heathkit H-19
       DEC VT52S0T7

Similar terminals or later models in the same series (eg S0T4Zenith Z-29S0T7, S0T4DEC VT100S0T7)
can also be tried.

If no suitable selection is available, you may need to manually specify the
features requested by the software. This program should help you determine what
control codes and escape sequences to use.

You can try out the codes in this program to better understand their effects.
For example, to try the sequence S0T6ESC ES0T7 (clear & home) press the S1T5BREAKS0T7 key
followed by a capital S1T5ES0T7. Pressing S1T5cursor leftS0T7 or S1T5cursor rightS0T7 will always reset
the terminal and change to the next/previous screen.[1;1H$The special keys provided by the ZX Spectrum Next are mapped as described here.
Some mappings are particularly helpful at the CP/M command line (or when
software uses the CP/M line editor), and these uses are also shown.

rSpecial keyu    rMappingu         rCP/M command line / line editoru
S1T5BREAKS0T7                          S0T6ESCS0T7 (can be used to type an escape sequence)
S1T5EDITS0T7           S1T5CTRL-WS0T7          recall last command (if used at start of line)
S1T5TRUE VIDEOS0T7     S1T5CTRL-SS0T7          stop screen scrolling (S1T5CTRL-QS0T7 resumes scrolling)
S1T5INV. VIDEOS0T7     S1T5CTRL-IS0T7          S0T6TAB
S0T7S1T5CAPS LOCKS0T7                      Turns on/off caps lock (not passed to software)
S1T5GRAPHS0T7          S1T5CTRL-GS0T7          delete character under cursor
S1T5DELETES0T7         S1T5CTRL-HS0T7          delete previous character
S1T5EXTENDS0T7         S1T5CTRLS0T7 key
S1T5Cursor leftS0T7    S1T5CTRL-AS0T7          left
S1T5Cursor rightS0T7   S1T5CTRL-FS0T7          right
S1T5Cursor upS0T7      S1T5CTRL-ES0T7          start new line without submitting command
S1T5Cursor downS0T7    S1T5CTRL-XS0T7          delete to start of line

The keymappings may also be helpful in some software packages. For example,
in S0T4WordStarS0T7 the cursor keys correspond to left & right by one word, and to
up & down by one line.[1;1H$The terminal displays characters using font files, which are in the standard
768-byte Spectrum character set format. You can replace these with your own
fonts if you wish. These files are located in the c:/nextzxos/cpm/system
directory in NextZXOS:
       S0T4NORMAL.FNTS0T7      standard font
       S0T4UNDER.FNTS0T7       underlined font
       S0T4ITALIC.FNTS0T7      italicised font
       S0T4ITAL_UND.FNTS0T7    italicised & underlined font
       S0T4GRAPHICS.FNTS0T7    special Z19/H19 graphics characters

The terminal responds to the following ASCII control characters, all of which
can be typed directly with the keyboard:
S1T5CTRL-GS0T7 (S1T5GRAPHS0T7)         S0T6BELS0T7 (ASCII 7), alert/bell
S1T5CTRL-HS0T7 (S1T5DELETES0T7)        S0T6BSS0T7  (ASCII 8), backspace
S1T5CTRL-IS0T7 (S1T5INV. VIDEOS0T7)    S0T6TABS0T7 (ASCII 9), tab
S1T5CTRL-JS0T7                 S0T6LFS0T7  (ASCII 10), line feed
S1T5CTRL-MS0T7 (S1T5ENTERS0T7)         S0T6CRS0T7  (ASCII 13), carriage return
S1T5CTRL-XS0T7                 S0T6CANS0T7 (ASCII 24), cancel current escape sequence
       (S1T5BREAKS0T7)         S0T6ESCS0T7 (ASCII 27), used to start escape sequences
       (S1T5CTRL-DELETES0T7)   S0T6DELS0T7 (ASCII 127), delete forwards

Most terminal facilities are accessed using S0T6ESCS0T7ape sequences.[1;1H$The following S0T4Zenith Z-19S0T7 escape sequences are supported, many of which are
also common to the S0T4DEC VT52S0T7 and other terminals.

In the lists of sequences, S0T6ESCS0T7 means the character code ASCII 27. The case of
letters is important. Note that sequences involving lower-case letters cannot
be directly typed at the command line because CP/M Plus automatically
converts all user input to upper case before executing it. To work around this
limitation, the S0T4ECHO.COMS0T7 utility is provided.

rSequenceu       rEffectu
S0T6ESC @S0T7          enter insert mode
S0T6ESC AS0T7          cursor up
S0T6ESC BS0T7          cursor down
S0T6ESC CS0T7          cursor right
S0T6ESC DS0T7          cursor left
S0T6ESC ES0T7          clear & home screen
S0T6ESC FS0T7          enter graphics mode (affects ASCII 94..126)
S0T6ESC GS0T7          leave graphics mode
S0T6ESC HS0T7          cursor home
S0T6ESC IS0T7          reverse index (move cursor up, scrolling if necessary)
S0T6ESC JS0T7          erase from cursor to end of screen
S0T6ESC KS0T7          erase from cursor to end of line[1;1H$S0T4Zenith Z-19S0T7 escape sequences (continued)

rSequenceu       rEffectu
S0T6ESC LS0T7          insert line
S0T6ESC MS0T7          delete line
S0T6ESC NS0T7          delete character under cursor
S0T6ESC OS0T7          leave insert mode

S0T6ESC YS0T7 T3UlT7V T3UcT7V      set cursor position to line T3UlT7V, column T3UcT7V (1,1 is top-left)
               (T3UlT7V and T3UcT7V are ASCII characters: add 32 to the desired value)
               eg to set cursor to line 1, column 50: S0T6ESC Y ! RS0T7

S0T6ESC bS0T7          erase from cursor to start of screen
S0T6ESC jS0T7          save cursor position
S0T6ESC kS0T7          restore cursor position
S0T6ESC lS0T7          erase entire line
S0T6ESC nS0T7          send cursor position report to input (in form S0T6ESC Y T3Ul cT7VS0T7)
S0T6ESC oS0T7          erase from cursor to start of line
S0T6ESC pS0T7          enter reverse video mode
S0T6ESC qS0T7          leave reverse video mode
S0T6ESC vS0T7          enable end-of-line wrapping
S0T6ESC wS0T7          disable end-of-line wrapping[1;1H$S0T4Zenith Z-19S0T7 escape sequences (continued)

rSequenceu       rEffectu
S0T6ESC x 1S0T7        enable 25th-line support (only reachable by S0T6ESC Y T3Ul cT7VS0T7)
S0T6ESC x 5S0T7        suppress cursor
S0T6ESC y 1S0T7        disable 25th-line support
S0T6ESC y 5S0T7        enable cursor
S0T6ESC zS0T7          reset terminal to default modes

Some additional escape sequences used by S0T4LocomotiveS0T7's version of CP/M for the
S0T4ZX Spectrum +3S0T7 are also supported. These are:

rSequenceu       rEffectu
S0T6ESC dS0T7          erase from cursor to start of screen
S0T6ESC eS0T7          enable cursor blob
S0T6ESC fS0T7          disable cursor blob
S0T6ESC rS0T7          enter underline mode
S0T6ESC uS0T7          leave underline mode
[1;1H$The following additional escape sequences are specific to the S0T4ZX Spectrum NextS0T7:

rSequenceu       rEffectu

S0T6ESC S S0T7T3UnT7V        set background colour (T3UnT7V is an ASCII digit from '0' to '7')
               eg for a blue background: S0T6ESC S 1S0T7

S0T6ESC S R S0T7T3UrT7V T3UgT7V T3UbT7V  redefine current ZX background colour to the RGB value
               (T3Ur g bT7V are ASCII digits from '0' to '7')
               eg redefine background colour to grey: S0T6ESC S R 1 1 1S0T7

S0T6ESC T S0T7T3UnT7V        set foreground colour (T3UnT7V is an ASCII digit from '0' to '7')
               eg for yellow text: S0T6ESC T 6S0T7

S0T6ESC T R S0T7T3UrT7V T3UgT7V T3UbT7V  redefine current ZX foreground colour to the RGB value
               (T3Ur g bT7V are ASCII digits from '0' to '7')
               eg redefine foreground colour to orange: S0T6ESC T R 7 4 1S0T7

S0T6ESC US0T7          enter italics mode
S0T6ESC VS0T7          leave italics mode
S0T6ESC WS0T7          make current colours the defaults, and 'wash' the screen[1;1H$A number of ANSI sequences are also supported by the terminal. In these
sequences, 1 or 2 parameters may be provided, labeled T3UnT7V and T3UmT7V. Such parameters
are any sequence of ASCII digits 0 to 7, eg '23'. Parameters are always
optional and if not provided, a default of 0 is used (unless stated otherwise).

rSequenceu       rEffectu
S0T6ESC cS0T7          reset terminal to default modes
S0T6ESC [ S0T7T3UnT7VS0T6 AS0T7      move cursor up T3UnT7V lines (default 1)
S0T6ESC [ S0T7T3UnT7VS0T6 BS0T7      move cursor down T3UnT7V lines (default 1)
S0T6ESC [ S0T7T3UnT7VS0T6 CS0T7      move cursor right T3UnT7V columns (default 1)
S0T6ESC [ S0T7T3UnT7VS0T6 DS0T7      move cursor left T3UnT7V columns (default 1)
S0T6ESC [ S0T7T3UnT7VS0T6 ES0T7      move cursor to start of T3UnT7V-th following line (default 1)
S0T6ESC [ S0T7T3UnT7VS0T6 FS0T7      move cursor to start of T3UnT7V-th preceding line (default 1)
S0T6ESC [ S0T7T3UnT7VS0T6 GS0T7      move cursor to T3UnT7V-th column of current line (default 1)
S0T6ESC [ S0T7T3UnT7VS0T6 ; S0T7T3UmT7VS0T6 HS0T7  move cursor to T3UnT7V-th line and T3UmT7V-th column (defaults 1)
S0T6ESC [ S0T7T3UnT7VS0T6 JS0T7      erase: if T3UnT7V=0, from cursor to end of screen
                      if T3UnT7V=1, from cursor to start of screen
                      if T3UnT7V=2 or 3, entire screen
S0T6ESC [ S0T7T3UnT7VS0T6 KS0T7      erase: if T3UnT7V=0, from cursor to end of line
                      if T3UnT7V=1, from cursor to start of line
                      if T3UnT7V=2, entire line[1;1H$ANSI escape sequences (continued)

rSequenceu       rEffectu
S0T6ESC [ S0T7T3UnT7VS0T6 LS0T7      insert T3UnT7V lines (default 1)
S0T6ESC [ S0T7T3UnT7VS0T6 MS0T7      delete T3UnT7V lines (default 1)
S0T6ESC [ S0T7T3UnT7VS0T6 @S0T7      insert T3UnT7V characters (default 1)
S0T6ESC [ S0T7T3UnT7VS0T6 PS0T7      delete T3UnT7V characters (default 1)
S0T6ESC [ S0T7T3UnT7VS0T6 ; S0T7T3UmT7VS0T6 fS0T7  move cursor to T3UnT7V-th line and T3UmT7V-th column (defaults 1)
S0T6ESC [ S0T7T3UnT7VS0T6 hS0T7      set mode: if T3UnT7V=4, turn on insert mode
                         if T3UnT7V=7, turn on autowrap mode
S0T6ESC [ S0T7T3UnT7VS0T6 lS0T7      reset mode: if T3UnT7V=4, turn off insert mode
                           if T3UnT7V=7, turn off autowrap mode
S0T6ESC [ sS0T7        save cursor position
S0T6ESC [ uS0T7        restore cursor position
S0T6ESC [ zS0T7        reset terminal[1;1H$ANSI escape sequences (continued)

rSequenceu       rEffectu
S0T6ESC [ S0T7T3UnT7VS0T6 mS0T7      select graphic rendition specified by mode T3UnT7V
                       if T3UnT7V=0: normal (reset all effects)
                       if T3UnT7V=3: italics on
                       if T3UnT7V=4: underline on
                       if T3UnT7V=7: reverse video on
                       if T3UnT7V=23: italics off
                       if T3UnT7V=24: underline off
                       if T3UnT7V=27: reverse video off
                       if T3UnT7V=30: foreground colour black
                       if T3UnT7V=31: foreground colour red
                       if T3UnT7V=32: foreground colour green
                       if T3UnT7V=33: foreground colour yellow
                       if T3UnT7V=34: foreground colour blue
                       if T3UnT7V=35: foreground colour magenta
                       if T3UnT7V=36: foreground colour cyan
                       if T3UnT7V=37: foreground colour white[1;1H$ANSI escape sequences (continued)

rSequenceu       rEffectu
S0T6ESC [ S0T7T3UnT7VS0T6 mS0T7      select graphic rendition specified by mode T3UnT7V (continued)
                       if T3UnT7V=40: background colour black
                       if T3UnT7V=41: background colour red
                       if T3UnT7V=42: background colour green
                       if T3UnT7V=43: background colour yellow
                       if T3UnT7V=44: background colour blue
                       if T3UnT7V=45: background colour magenta
                       if T3UnT7V=46: background colour cyan
                       if T3UnT7V=47: background colour white[1;1H$Some ANSI sequences clash with standard escape sequences provided with the
S0T4Zenith Z-19S0T7. If you have an application that requires the ANSI meaning for
these sequences, you can enter ANSI compatibility mode.

The following sequences enter or leave ANSI compatibility mode:

rSequenceu       rEffectu
S0T6ESC <S0T7          enter ANSI compatibility mode
S0T6ESC [ ? 2 hS0T7    leave ANSI compatibility mode


When in ANSI compatibility mode, these sequences have the following meanings:


rSequenceu       rEffectu
S0T6ESC DS0T7          move cursor down 1 line, scrolling if necessary
S0T6ESC ES0T7          move cursor to start of next line, scrolling if necessary
S0T6ESC MS0T7          move cursor up 1 line, scrolling if necessary
[1;1H$